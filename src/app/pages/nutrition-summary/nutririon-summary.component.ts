import { Component, OnInit } from "@angular/core";
import { NutritionService } from "src/app/services/nutrition.service";

@Component({
    templateUrl:'nutririon-summary.component.html',
    styleUrls:['nutririon-summary.component.scss']
})
export class NutritionSummaryComponent implements OnInit {

    nutritionData:any;
    displayedColumns: string[] = ['label', 'quantity', 'unit'];
    dataSource :any;
    totalNutrition: any[] = [];

    constructor(private service: NutritionService){}

    ngOnInit(){
        this.getData();
    }

    getData(){
        this.service.getNutritionData().subscribe(data => {
            console.log(data);
            let ca = data.totalNutrients.CA;
            let cole = data.totalNutrients.CHOLE;
            let fat = data.totalNutrients.FAT;
            let fe = data.totalNutrients.FE;
            let k = data.totalNutrients.K;
            let na = data.totalNutrients.NA;
            let fibtg = data.totalNutrients.FIBTG;
            let arr = [ca, cole,fat,fe,k,na,fibtg]

            this.nutritionData = data;
            this.totalNutrition = arr;
            console.log(this.totalNutrition)
            this.dataSource = this.totalNutrition;

        });
    }

}
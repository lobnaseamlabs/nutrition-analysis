import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NutritionService } from "src/app/services/nutrition.service";

@Component({
    templateUrl:'nutrition-form.component.html',
    styleUrls:['nutrition-form.component.scss']
})
export class NutritionFormComponent implements OnInit{

    nutritionForm!: FormGroup;

    constructor(private fb:FormBuilder, private service:NutritionService, private router:Router){}

    ngOnInit(){
        this.initNutritionForm();
    }

    initNutritionForm(){
        this.nutritionForm = this.fb.group({
            title: '',
            ingr: this.fb.array([this.initNewIngr()]) ,
        });
    }

    get ingr() : FormArray {
        return this.nutritionForm.get("ingr") as FormArray
    }

 
    initNewIngr() {
        return this.fb.control('', Validators.required);
    }

    addIngr() {
        this.ingr.push(this.initNewIngr());
    }

    removeIngr(i:number) {
        this.ingr.removeAt(i);
    }

    onSubmit() {
        console.log(this.nutritionForm.value);
        let body = this.nutritionForm.value;
        this.service.postNutritionData(body).subscribe(res => {
            console.log(res);
            this.service.setNutritionData(res);
            this.router.navigateByUrl('/nutrition-summary')

        })
    }

}
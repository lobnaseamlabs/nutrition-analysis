import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'; 
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class NutritionService {
    base_url='https://api.edamam.com/api/';
    headers = new HttpHeaders();
    params = new HttpParams()
    .set('app_id', '90223acd')
    .set('app_key','5d033c30f1df8ea7b5e1721ac429d6ad' )
    // .set('ingr', 'pasta')
    .set('nutrition-type','logging');
    nutritionData: BehaviorSubject<any> = new BehaviorSubject(null);
    constructor(private http: HttpClient){
        this.headers.append('Content-Type', 'application/json');
    }

    setNutritionData(nutritionData: any) {
        this.nutritionData.next(nutritionData);
    }
    getNutritionData(): Observable<any> {
        return this.nutritionData.asObservable();
    }

    // getNutritionData(){
    //     return this.http.get(this.base_url,{headers:this.headers , params: this.params});
    // }
    postNutritionData(body: any){
        return this.http.post(this.base_url+'nutrition-details', body,{headers:this.headers , params: this.params})
    }
}
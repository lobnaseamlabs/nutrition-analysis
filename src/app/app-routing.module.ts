import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NutritionFormComponent } from './pages/nutrition-form/nutrition-form.component';
import { NutritionSummaryComponent } from './pages/nutrition-summary/nutririon-summary.component';



const routes: Routes = [
  {
    path:'nutrition-log',
    component:NutritionFormComponent
  },
  {
    path:'nutrition-summary',
    component:NutritionSummaryComponent
  },
  {
    path: '',
    redirectTo: 'nutrition-log',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'nutrition-log',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

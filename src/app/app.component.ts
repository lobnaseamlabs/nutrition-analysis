import { Component, OnInit } from '@angular/core';
import { NutritionService } from './services/nutrition.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'nutrition-analysis';

  constructor(private service: NutritionService){}

  ngOnInit(){
    // this.service.getNutritionData().subscribe(res => {
    //   console.log(res)
    // })
  }
}
